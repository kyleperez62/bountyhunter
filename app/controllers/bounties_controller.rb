class BountiesController < ApplicationController

  layout 'bounties'

  before_action :authenticate_admin!
  before_action :set_bounty, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @bounties = Bounty.all.order("id ASC").page(params[:page])
    respond_with(@bounties)
  end

  def apply
    #binding.pry
    checked_bounties_id = params[:chk]
    status = params[:status]
    is_status = false
    is_status = true if status == 'Publish'
    for bounty_id in checked_bounties_id do
      bounty = Bounty.find(bounty_id)
      bounty.status = is_status
      bounty.save
    end
    if is_status
      flash[:notice] = "Selection(s) have been published."
    else
      flash[:notice] = "Selection(s) have been privated."
    end
    redirect_to bounties_path
  end

  def show
    respond_with(@bounty)
  end

  def new
    @bounty = Bounty.new
    respond_with(@bounty)
  end

  def edit
    @starting_date = @bounty[:starting_date].strftime("%e %b %Y") unless @bounty[:starting_date] == nil
    @ending_date = @bounty[:ending_date].strftime("%e %b %Y") unless @bounty[:ending_date] == nil
    @attachments = Attachment.find_by_bounty_id(@bounty.id)
    binding.pry
  end

  def create
    @bounty = Bounty.new(bounty_params)

    #binding.pry
    @bounty[:starting_date] = params[:starting_date]
    @bounty[:ending_date] = params[:ending_date]

    if @bounty.save
      save_attachment @bounty
      redirect_to bounties_path, flash:{ :notice => "Created successfully."}
    else
      render :edit
    end
    #respond_with(@bounty)
  end

  def update
    #binding.pry
    if @bounty.update_attributes(bounty_params)
      attachment_title_list = params[:attachment_title]
      attachment_file_list = params[:attachment_file]
      is_changed_attachment = false
      for i in 0..attachment_title_list.length - 1
        if attachment_title_list[i] != "" and attachment_file_list[i] != ""
          is_changed_attachment = true
        end
      end
      
      if is_changed_attachment
        @bounty.delete_attachment
        save_attachment @bounty
      end
      
      redirect_to bounties_path, flash:{ :notice => "Updated successfully."}
    else
      render :edit
    end
    
    #respond_with(@bounty)
  end

  def save_attachment bounty
    attachment_title_list = params[:attachment_title]
    attachment_file_list = params[:attachment_file]
    for i in 0..attachment_title_list.length - 1
      if attachment_title_list[i] != "" and attachment_file_list[i] != ""
        attachment = Attachment.new({:bounty_id => bounty.id, :title => attachment_title_list[i], :address => attachment_file_list[i]})
        attachment.save
      end
    end
  end



  def destroy
    @bounty.destroy
    redirect_to bounties_path, flash:{ :notice => "Deleted successfully."}
  end

  private
    def set_bounty
      @bounty = Bounty.find(params[:id])
    end

    def bounty_params
      #params[:bounty]
      params.require(:bounty).permit([:bounty_title, :bounty_url, :status, :convicted, :progress, :bounty_graphic, 
                                        :bounty_name, :email, :short_desc, :summary, 
                                        :background, :btc_address, :discussion, :remote_btc_image_url])
    end
end
