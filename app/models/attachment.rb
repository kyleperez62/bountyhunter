class Attachment < ActiveRecord::Base

  belongs_to :bounty

  mount_uploader :address, AvatarUploader

end
