class Bounty < ActiveRecord::Base
  has_many :attachments, dependent: :destroy

  validates_presence_of :bounty_url
  validates_presence_of :bounty_name
  validates_presence_of :email
  validates :email, uniqueness: true, on: :create
  validates_format_of :email, :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i

  mount_uploader :bounty_graphic, AvatarUploader
  mount_uploader :btc_image, AvatarUploader

  def delete_attachment
    old_attachments = Attachment.find_by_bounty_id(self.id)
    old_attachments.delete unless old_attachments == nil
  end
  
end
