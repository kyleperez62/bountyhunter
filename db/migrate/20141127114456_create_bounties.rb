class CreateBounties < ActiveRecord::Migration
  def change
    create_table :bounties do |t|
      t.boolean :status,              default: false
      t.string :bounty_title,         default: ""
      t.string :bounty_url,           default: ""
      t.boolean :convicted,           default: false
      t.boolean :progress,            default: false
      t.string :bounty_graphic,       default: ""
      t.string :bounty_name,          default: ""
      t.string :email,                default: ""
      t.string :short_desc,           default: ""
      t.text :summary,                default: ""
      t.datetime :starting_date
      t.datetime :ending_date
      t.text :background,             default: ""
      t.string :btc_address
      t.string :discussion
      t.float :bounty_amount,         default: 0
      t.timestamps
    end
  end
  
end
