class CreateAttachments < ActiveRecord::Migration
  def change
    create_table :attachments do |t|
      t.integer "bounty_id"
      t.string "title",          :default => ""
      t.string "address",        :default => ""
      t.timestamps
    end
  end
end
