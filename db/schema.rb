# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20141130204827) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "admins", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "admins", ["email"], name: "index_admins_on_email", unique: true, using: :btree
  add_index "admins", ["reset_password_token"], name: "index_admins_on_reset_password_token", unique: true, using: :btree

  create_table "attachments", force: true do |t|
    t.integer  "bounty_id"
    t.string   "title",      default: ""
    t.string   "address",    default: ""
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "bounties", force: true do |t|
    t.boolean  "status",         default: false
    t.string   "bounty_title",   default: ""
    t.string   "bounty_url",     default: ""
    t.boolean  "convicted",      default: false
    t.boolean  "progress",       default: false
    t.string   "bounty_graphic", default: ""
    t.string   "bounty_name",    default: ""
    t.string   "email",          default: ""
    t.string   "short_desc",     default: ""
    t.text     "summary",        default: ""
    t.datetime "starting_date"
    t.datetime "ending_date"
    t.text     "background",     default: ""
    t.string   "btc_address"
    t.string   "discussion"
    t.float    "bounty_amount",  default: 0.0
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "btc_image"
  end

end
