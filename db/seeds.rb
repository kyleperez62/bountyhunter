# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


account = Admin.create(email: "kyle.perez1985@gmail.com", password: "baishiwanA75")
account = Admin.create(email: "demo@email.com", password: "password")


bounty = Bounty.create(bounty_title: "Test1", btc_address: "asfsfdsfgwers", bounty_amount: 124.67)
bounty = Bounty.create(bounty_title: "Test2", btc_address: "asfsfdsfgwers", bounty_amount: 124)
bounty = Bounty.create(bounty_title: "Test3", btc_address: "asfsfdsfgwers", bounty_amount: 12)
bounty = Bounty.create(bounty_title: "Test4", btc_address: "asfsfdsfgwers", bounty_amount: 124.6)
bounty = Bounty.create(bounty_title: "Test5", btc_address: "asfsfdsfgwers", bounty_amount: 124.67)
bounty = Bounty.create(bounty_title: "Test6", btc_address: "asfsfdsfgwers", bounty_amount: 124.67)
bounty = Bounty.create(bounty_title: "Test7", btc_address: "asfsfdsfgwers", bounty_amount: 124.67)
bounty = Bounty.create(bounty_title: "Test8", btc_address: "asfsfdsfgwers", bounty_amount: 13456.00)
bounty = Bounty.create(bounty_title: "Test9", btc_address: "asfsfdsfgwers")
bounty = Bounty.create(bounty_title: "Test10", btc_address: "asfsfdsfgwers")
bounty = Bounty.create(bounty_title: "Test11", btc_address: "asfsfdsfgwers")
bounty = Bounty.create(bounty_title: "Test12", btc_address: "asfsfdsfgwers")
bounty = Bounty.create(bounty_title: "Test13", btc_address: "asfsfdsfgwers", bounty_amount: 12.67)
bounty = Bounty.create(bounty_title: "Test14", btc_address: "asfsfdsfgwers")
bounty = Bounty.create(bounty_title: "Test15", btc_address: "asfsfdsfgwers")
bounty = Bounty.create(bounty_title: "Test16", btc_address: "sdfqwr", bounty_amount: 11.67)
bounty = Bounty.create(bounty_title: "Test17", btc_address: "hhgffdh")
bounty = Bounty.create(bounty_title: "Test18", btc_address: "wert")
bounty = Bounty.create(bounty_title: "Test19", btc_address: "cvnncb", bounty_amount: 12.67)
